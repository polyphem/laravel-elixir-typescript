var gulp = require('gulp');
var Elixir = require('laravel-elixir');
var ts = require('gulp-typescript');

var $ = Elixir.Plugins;
var config = Elixir.config;

Elixir.extend('typescript', function(output, baseDir, options) {
    var paths = prepGulpPaths('/**/*.ts', baseDir, output);

    new Elixir.Task('typescript', function() {
        var typescriptOptions = options || config.js.typescript.options;

        return gulpTask.call(this, paths, typescriptOptions);
    })
    .watch(paths.src.baseDir + '/**/*.ts')
    .ignore(paths.output.path);
});

/**
 * Trigger the Gulp task logic.
 *
 * @param {GulpPaths}   paths
 * @param {object|null} typescript
 */
var gulpTask = function(paths, typescript) {
    this.log(paths.src, paths.output);

    return (
        gulp
            .src(paths.src.path)
            .pipe($.if(config.sourcemaps, $.sourcemaps.init()))
            .pipe(ts(typescript))
            .on('error', function(e) {
                new Elixir.Notification().error(e, 'Typescript Compilation Failed!');
                this.emit('end');
            })
            .pipe($.concat(paths.output.name))
            .pipe($.if(config.production, $.uglify({compress: { drop_console: true }})))
            .pipe($.if(config.sourcemaps, $.sourcemaps.write('.')))
            .pipe(gulp.dest(paths.output.baseDir))
            .pipe(new Elixir.Notification('Typescript Compiled!'))
    );
};

/**
 * Prep the Gulp src and output paths.
 *
 * @param  {string|Array} src
 * @param  {string|null}  baseDir
 * @param  {string|null}  output
 * @return {GulpPaths}
 */
var prepGulpPaths = function(src, baseDir, output) {
    return new Elixir.GulpPaths()
        .src(src, baseDir || config.get('assets.js.folder'))
        .output(output || config.get('public.js.outputFolder'), 'all.js');
};