## Usage

This Laravel Elixir extension allows you to compile Typescript.

## Installation

First, pull in the extension through NPM.

```
npm install --save @polyphem/laravel-elixir-typescript
```

Next, add it to your Elixir-enhanced Gulpfile, like so:

```js
var elixir = require('laravel-elixir');

require('@polyphem/laravel-elixir-typescript');

elixir(function(mix) {
mix.typescript();
});
```

That's it! You're all set to go!

## Usage

Assuming you write...

```js
elixir(function(mix) {
mix.typescript();
});
```

...this will compile your `resources/assets/js/**/*.ts` file to `./public/js/all.js`.

If you'd like to set a different output directory, you may pass a second argument to the `typescript()` method, like so:

```js
mix.typescript('public/js', 'resources/assets/js/')
```

Finally, if you want to override the Typescript plugin options, you may pass an object as the third argument.

```js
mix.typescript(null, null, {});
```